// Global components
const code_output_component = import('./components/code-output.component.min.js');

const app = new Vue({
  el: '#pageWrapper',
  delimiters: ['${', '}'],

  data:{
      modal: '',

      searchText: '',
      searchResults: [],
      searchDone: false,
      searchReady: false,
      fuse: undefined,

      showMobileMenu: false,
  },

  components: {
    'code-output': () => code_output_component,
    'audacity-label-to-cue': () => audacity_label_to_cue? audacity_label_to_cue: {},
  },

  beforeMount() {

  },

  mounted() {
    window.addEventListener('keypress', e => {
      if (this.modal !== ''){ return; }

      switch(e.code){
        case 'KeyS':
          this.toggleSidebar();
          break;
        case 'KeyF':
          this.openSearch();
          break;
        case 'KeyQ':
          this.backToSection();
          break;
        default:
          break;
      }

    });


    // When the user scrolls down from the top of the document, show the button
    window.onscroll = () => {
      this.scrollFunction();
    };

  },

  watch: {
    searchText: function (val, oval){
      if (val.length === 0){
        this.searchDone = false;
        return;
      }
      if (val.length >= 3){
        this.runSearch(val);
      }
    }
  },

  methods: {

    openSearch: async function (){
      this.modal = 'search';
      await this.loadSearch();
      setTimeout( () => {
        document.getElementById('searchInputBox').focus();
      }, 200);
    },

    closeSearch: function (){
      this.modal = '';
      this.searchText = '';
    },

    toggleMobileMenu: function(){
      this.showMobileMenu = !this.showMobileMenu;
    },

    getFile: function(path, cb){

      fetch(path)
      .then( (res) => {
        // console.log(res);
        if (!res.ok) {
          console.log('Error fetching ' + path);
          if (cb) cb('');
        }
        return res.json();
      })
      .then( (data) => {
        console.log('Fetched ' + path);
        // console.log(data);
        if (cb) cb(data);
      })
      .catch( (err) => {
        if (err){
          console.log(err);
        }
        if (cb) cb('');
      });

    },

    loadSearch: async function(){

      if (this.searchReady && this.fuse){
        return;
      }

      let res = await new Promise( r => {
        this.getFile('/index.json', (data) => {
          var options = {
            shouldSort: true,
            location: 0,
            distance: 100,
            threshold: 0.4,
            minMatchCharLength: 3,
            keys: [
              'title',
              'permalink',
              'contents',
              'tags'
              ]
          };

          try {
            this.fuse = new Fuse(data, options);
            this.searchReady = true;
          } catch (err){
            if (err){
              this.fuse = undefined;
              this.searchReady = false;
              console.log(err);
              r();
            }
          }

          // Resolve promise when done
          r();
        });
      });
      
      return;
    },

    runSearch: async function(term){

      this.searchDone = false;
      this.searchResults = [];

      let loadTry = 0;

      while (loadTry < 3 && (!this.searchReady || !this.fuse)){
        loadTry++;
        await this.loadSearch();
      }

      if (!this.searchReady || !this.fuse){
        this.searchDone = true;
        return;
      }

      let results = this.fuse.search(term); // the actual query being run using fuse.js

      for (let i=0; i < results.length; i++){
        if (i >= 10){ break; }

        // let item = this.extractSearchMatch(term, results[i].item);
        let item = results[i].item;

        this.searchResults.push(item);
      }

      this.searchDone = true;
    },

    extractSearchMatch: function(term, item){
      term = term.toLowerCase();

      let contents = item.contents;
      let contentsArray = [];
      let posArray = [];
      let nextP = 0;

      // Find all the positions of matching string
      while (true) {
        let p = contents.substr( nextP, contents.length - nextP).search(new RegExp( '/'+term+'/', 'i'));
        console.log(p);
        if (p < 0 || posArray.length > 100 ){ break; }
        posArray.push(p);

        nextP = p + 1;

        if ( nextP >= contents.length ){ break; }
      }


      let start = 0;
      let end = 0;
      const span = 50;

      // Get all the substrings of the matching term
      for (let i=0; i < posArray.length; i++){
        const p = posArray[i];
        
        // Get the beginning position
        start = p - span;
        if (start < 0){ start = 0; }
        
        // Get the ending position
        let j = i + 1;
        let extend = 0;
        end = p + span;
        if (end >= contents.length){ end = contents.length - 1; }

        while (j < posArray.length && posArray[j] <= end){
          end = posArray[j] + span;
          if (end >= contents.length){
            end = contents.length - 1;
            break;
          }

          j++;
          extend++;
        }

        i += extend;

        contentsArray.push( contents.substr( start, end - start) );
        if (i < posArray.length - 1){ contentsArray.push(' ... '); }
      }

      if (contentsArray.length > 0){
        item.contents = contentsArray.join();
      } else {
        item.contents = item.contents.substr(0, 500);
      }

      return item;
    },

    openFirstSearchResult: function(){
      if (this.searchResults.length <= 0){
        return;
      }

      let url = this.searchResults[0].permalink;
      window.location.href = url;
    },

    backToTop: function(){
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    },

    scrollFunction: function() {
      //Get the button
      let topButton = document.getElementById("backToTopBtn");

      if (
        document.body.scrollTop > 50 ||
        document.documentElement.scrollTop > 50
      ) {
        topButton.style.display = "block";
      } else {
        topButton.style.display = "none";
      }
    },

    backToSection: function() {
      let e = document.getElementById("sectionUrl");
      if (!e){ return; }

      let sectionUrl = e.href;
      if (!sectionUrl || sectionUrl === ''){ return; }

      window.location.href = sectionUrl;
    }

  }
});
