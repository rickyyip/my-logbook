export default {

name: 'code-output',

props: {
    label: String,
    relative: Boolean,
},

data(){
    return {
        show: false,
    }
},

template: /*html*/`
<div class="bottom-4 flex flex-col justify-center" :class="relative? 'relative': null">
    <slot v-if="show"></slot>
    <button class="rounded-lg border-b-2 border-gray-600" :class="show? 'text-gray-500 hover:text-gray-300': 'hover:text-yellow-300'" @click="show = !show"><i class="fas fa-caret-down" v-if="!show"></i><i class="fas fa-caret-up" v-if="show"></i> <span class="mx-2" v-text="label || 'Output'"></span> <i class="fas fa-caret-down" v-if="!show"></i><i class="fas fa-caret-up" v-if="show"></i></button>
</div>
`

}
