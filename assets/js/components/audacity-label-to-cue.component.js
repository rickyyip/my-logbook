// Global components
const code_output_component = import('./code-output.component.min.js');

export default {

name: 'audacity-label-to-cue',

data(){
    return {
        title: '',
        performer: '',
        filename: '',
        labelData: '',
        cueOutput: '',
        track: 0,

        copied: false,
    }
},

components: {
    'code-output': () => code_output_component,
},

template: /*html*/`
<div class="w-full">
    <div class="grid grid-cols-1 md:grid-cols-2 gap-2">
        <div>
            <span class='block'>Labels</span>
            <textarea class="w-full h-64 bg-gray-700 text-gray-50 rounded-lg" v-model="labelData" placeholder="Paste labels here"></textarea>
            <div class="flex justify-center items-center md:justify-end my-4">
                <button class="py-2 px-4 bg-blue-500 active:bg-blue-400 rounded-md" @click="convertToCue()">Convert</button>
            </div>
            <code-output :label="'Example'">
<pre><code>\
0.000000	0.000000	1
123.456789	123.456789	2
456.123456	456.123456	3
</code></pre>
            </code-output>
        </div>

        <div>
            <span class='block'>Cue Sheet</span>
            <textarea class="w-full h-64 bg-gray-900 text-gray-50 rounded-lg" v-model="cueOutput" placeholder="Output" disabled></textarea>
            <div class="flex justify-center items-center md:justify-start my-4">
                <button class="block md:inline py-2 px-4 rounded-md active:bg-green-500" @click="copyCue()" :class="cueOutput === ''? 'bg-gray-700': 'bg-green-700'">
                    <span class="text-gray-300" v-if="copied">Copied</span>
                    <span v-else>Copy</span>
                </button>
            </div>
            <code-output :label="'Example'">
<pre><code>\
REM GENRE Unknown
REM DATE 2021
TITLE "Unknown Title"
PERFORMER "Unknown Performer"
FILE "Unknown.file" WAVE
REM NEW-TRACK
  TRACK 01 AUDIO
  TITLE "1"
  PERFORMER "Unknown Performer"
  INDEX 01 00:00:00
REM NEW-TRACK
  TRACK 02 AUDIO
  TITLE "2"
  PERFORMER "Unknown Performer"
  INDEX 01 02:03:34
REM END-TITLE
</code></pre>
            </code-output>
        </div>
    </div>

</div>
`,

methods: {
    convertToCue(){
        let data = this.labelData.replace('\r\n', '\n');
        const lines = data.split('\n');

        // Reset data
        var cueSheet = '';
        this.cueOutput = '';
        this.track = 0;
        
        this.copied = false;
        
        // let timestamp = 0;

        cueSheet += 'REM GENRE Unknown\n';
        cueSheet += `REM DATE ${new Date().getFullYear()}\n`;
        cueSheet += `TITLE "${this.title || 'Unknown Title'}"\n`;
        cueSheet += `PERFORMER "${this.performer || 'Unknown Performer'}"\n`;
        cueSheet += `FILE "${this.filename || 'Unknown.file'}" ${this.fileFormat || 'WAVE'}\n`;
        
        for (let i=0; i < lines.length; i++){
            const regexp = new RegExp(/^\s*([0-9\.]+)\s+([0-9\.]+)\s+([^\s]+)\s*$/);
            let match = regexp.exec(lines[i]);
            if (!match || match.length !== 4){
                console.log(`Skipping line ${i+1}`);
                continue;
            }

            let start = 0;
            let end = 0;
            let label = '';

            try {
                start = parseFloat(match[1]);
                end = parseFloat(match[2]);
                label = match[3];
            } catch(err){
                console.log(`Error: line ${i+1}: ${err}`);
                return;
            }

            this.track++;
            let timestamp = start;

            let minute = Math.trunc( timestamp / 60 );
            let second = Math.trunc( timestamp - minute*60 );
            let frame = Math.round( timestamp % 1 * 0.75 * 100 );

            cueSheet += 'REM NEW-TRACK\n';
            cueSheet += `  TRACK ${this.track < 10? '0' + this.track : this.track} AUDIO\n`;
            cueSheet += `  TITLE "${label}"\n`;
            cueSheet += `  PERFORMER "${this.performer || 'Unknown Performer'}"\n`;
            cueSheet += `  INDEX 01 ${minute < 10? '0'+minute: minute}:${second < 10? '0'+second: second}:${frame < 10? '0'+frame: frame}\n`;
        }

        cueSheet += 'REM END-TITLE\n';

        if (this.track > 0) this.cueOutput = cueSheet;
    },

    async copyCue(){
        if (this.cueOutput === '') return;

        navigator.clipboard.writeText(this.cueOutput);
        this.copied = true;

        await new Promise(r => setTimeout(r,1000));

        this.copied = false;
    }
}

}   // End of component
