var toc = document.getElementById('TableOfContents');

if (toc){

  var a = toc.getElementsByTagName('a');

  for (let i=0; i < a.length; i++){
      a[i].classList.add('hover:text-yellow-200');
  }

}


var codeOutputs = document.getElementsByClassName('language-output');

if (codeOutputs){
  for (let i=0; i < codeOutputs.length; i++){
    // code
    let el = codeOutputs[i];
    // pre
    let parent = el.parentElement;
    let parentNode = parent.parentNode;
    // div
    let wrapper = document.createElement('div');
    parentNode.replaceChild(wrapper, parent);
    wrapper.appendChild(parent);

    parent.style = "color: rgb(248, 248, 242);";

    // parent = parent.parentElement;
    wrapper.classList.add('overflow-x-auto');



    wrapper.outerHTML = wrapper.outerHTML.replace(/div/g, 'code-output');
  }
}
