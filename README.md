# Ricky's Logbook

## Description

This repository contains the source code of the Logbook.


## Usage

The website is compiled using [Hugo](https://gohugo.io/) and several other tools managed by NPM.

Requirements:
1. NPM v14+ / Yarn v1.22+

Installation:
1. Clone this repository.
2. Navigate to the project root folder.
3. Run: `yarn install`

Build project:
- Build website: `yarn build`
    - Compiled files are stored in `./public` folder.

Or, run a local server at http://localhost:1313: `yarn dev`


## License

### Contents inside the `./content` folder are published under the [Creative Commons Attribution 4.0 International (CC BY 4.0) License](https://creativecommons.org/licenses/by/4.0/).

You are allowed to:
    - Share — copy and redistribute the material in any medium or format.
    - Adapt — remix, transform, and build upon the material for any purpose, even commercially. 

Under the following terms:
    - Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. 
    - No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits. 

Notices:
    - You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation. 
    - No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material. 


### Source code outside of the `./content` folder are published under the [ISC license](https://www.isc.org/licenses/)

**Text of the ISC License:**

Copyright © 2004-2013 by Internet Systems Consortium, Inc. (“ISC”)
Copyright © 1995-2003 by Internet Software Consortium

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED “AS IS” AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

