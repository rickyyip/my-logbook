---
title: Credits
description: Thanks to these amazing resources
weight: 999
---

Tools:
- [Hugo](https://gohugo.io/)
- [Tailwind CSS](https://tailwindcss.com)
- [Vue.js](https://vuejs.org/)
- [Fuse.js](https://fusejs.io/)

Images and icons:
- [Font Awesome](https://fontawesome.com/)
- [Flaticon](https://www.flaticon.com/)

