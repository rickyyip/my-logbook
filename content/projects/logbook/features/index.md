---
title: Site features
description: Some handy features to navigate this logbook
weight: 10
---

# Shortcut Keys:

Main screen:
- Shift + S: Toggle sidebar
- Shift + F: Toggle search dialogue
- Shift + Q: Go to parent section (if exists)

Search dialogue:
- Esc: Close search dialogue
- Enter: Open first search result
