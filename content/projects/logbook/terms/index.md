---
title: Terms and Privacy
description: By using this website, you agree to these terms and policies.
weight: 1
---

Contents on this website are provided as-is, with absolutely no warranty to the extent permitted by applicable law.

This website does not use cookies or trackers.

This website may be hosted on GitLab, which may have their own privacy policies. Details: [https://about.gitlab.com/privacy/](https://about.gitlab.com/privacy/)
