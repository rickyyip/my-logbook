---
title: New Site Logo
date: 2021-08-21
thumbnail: /img/RY_logo_ring_500.png
hideSingleThumbnail: true
---

# Brand new design of site logo

![RY](/img/RY_logo_ring_500.png)
