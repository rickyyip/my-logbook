---
title: Tailwind CSS and Dark Mode
date: 2021-08-12
---

Logbook now uses Tailwind CSS, instead of Bootstrap. Overall look is now in dark mode by default.

